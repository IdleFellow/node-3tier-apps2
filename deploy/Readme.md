<h1>Deployment section</h1>
<p>
All the application deployments on the Kubernetes cluster are achieved through the Helm tool.<br/>
This section contains the charts needed to build and install the application components.
</p>
<h4>
Requirements
</h4>
<p>
<ul>
<li>
<b>Helm</b>: to build and install the application packages
</li>
</ul>
</p>
<h4>
Usage
</h4>
<p>
The application charts included in this section are:
<ul>
<li>
    <b>node-3tier-db</b>: The database application
</li>
<li>
    <b>node-3tier-app2</b>: The main Node.js application itself
</li>
<li>
    <b>blue-green-svc</b>: The services used to access the application from outside
</li>
</ul>
</p>
<p>
You are not supposed to manage the Helm charts directly. The application packages are built and deployed by the Jenkins deployment pipeline.
</p>
<p>
As a reference, this document provides the details on how the charts are organised and how to invoke Helm commands to manage them.
</p>
<p>
The charts are configurable modifying the configuration options included in the files <code>values.yaml</code><br/>
These files are organised with a default section (at the top level of the yaml dictionary) and another section labelled <code>prod</code><br/>
The values contained in the <code>prod</code> section are used when the chart is invoked passing the parameter <code>environment=prod</code> and are meant to be used in a production deployment.
</p>
<p>
The Helm installation command can be lauched with the following syntax:<br/>
<code>helm --namespace=&lt;namespace&gt; --set environment=&lt;environment&gt; install &lt;application name&gt;-&lt;environment&gt; &lt;application name&gt;</code><br/>
The parameters to be used are the following:
<ul>
<li>
    <b>namespace</b>: The Kubernetes namespace to deploy the application into
</li>
<li>
    <b>environment</b>: The application environment. If the value <code>prod</code> is provided, the application will be configured for a production deployment
</li>
<li>
    <b>application name</b>: the name of the chart to be deployed
</li>
</ul>
</p>
<p>
<b>Please note</b> that before using Helm to install the application, you must already have created the Docker images. Refer to the <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/docker">Docker section</a> for instructions on how to do it.
</p>
