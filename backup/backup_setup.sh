#!/bin/bash

NS=backup
ENCRYPTION_PASSWORD=password
SCHEDULE="2 0 * * *"
SNAPSHOTS_TO_KEEP=5
DB_NS=prod
DB_SECRET=db-secret-conf-node-3tier
DB_SERVICE=db-node-3tier
DB_PORT=5432

pushd ${0%/*}

# Install stash operator and postgres addon
/usr/sbin/helm repo add appscode https://charts.appscode.com/stable/
/usr/sbin/helm repo update
/usr/sbin/helm install stash-operator appscode/stash \
  --version v0.9.0-rc.6 \
  --namespace kube-system
/usr/sbin/helm install stash-postgres appscode/stash-postgres  --namespace kube-system

# delete old resources
kubectl delete ns "$NS" 2>/dev/null
kubectl delete BackupBlueprint postgres-backup-blueprint 2>/dev/null

# create namespace
kubectl create ns "$NS"

# Create a secret for the s3 repository
kubectl create secret generic -n "$NS" s3-secret \
    --from-literal=RESTIC_PASSWORD="$ENCRYPTION_PASSWORD" \
    --from-literal=AWS_ACCESS_KEY_ID=$(echo -n $(sed -ne 's|^aws_access_key_id = \(.*\)|\1|p' ~/.aws/credentials)) \
    --from-literal=AWS_SECRET_ACCESS_KEY=$(echo -n $(sed -ne 's|^aws_secret_access_key = \(.*\)|\1|p' ~/.aws/credentials))

# Create the backup blueprint
REGION=$(sed -ne 's|^region = \(.*\)|\1|p' ~/.aws/config)
kubectl -n "$NS" apply -f - <<EOF
apiVersion: stash.appscode.com/v1beta1
kind: BackupBlueprint
metadata:
  name: postgres-backup-blueprint
spec:
  backend:
    s3:
      endpoint: s3.amazonaws.com
      bucket: node-3tier-app2-backup
      prefix: /backup
      region: $REGION
    storageSecretName: s3-secret
  task:
    name: postgres-backup-11.2
  schedule: "$SCHEDULE"
  retentionPolicy:
    name: "keep-last-$SNAPSHOTS_TO_KEEP"
    keepLast: $SNAPSHOTS_TO_KEEP
    prune: true
EOF

# Create a secret to access the database
kubectl get secret "$DB_SECRET" --namespace="$DB_NS" --export -o yaml 2>/dev/null | \
  sed -e 's|password:|POSTGRES_PASSWORD:|;s|user:|POSTGRES_USER:|' | \
  kubectl apply --namespace="$NS" -f -

# Create the appbinding
kubectl -n "$NS" apply -f - <<EOF
apiVersion: appcatalog.appscode.com/v1alpha1
kind: AppBinding
metadata:
  annotations:
    stash.appscode.com/backup-blueprint: postgres-backup-blueprint
  name: db-node-3tier-stash-binding
spec:
  clientConfig:
    service:
      name: $DB_SERVICE.$DB_NS
      port: $DB_PORT
      path: /
      scheme: postgresql
      query: sslmode=disable
  secret:
    name: db-secret-conf-node-3tier
  type: postgres
  version: "11.2"
EOF

popd

echo "backup configuration finished"
