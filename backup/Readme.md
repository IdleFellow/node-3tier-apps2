<h1>Backup section</h1>
<p>
This section contains the information about the backup process.
</p>
<h4>
Requirements
</h4>
<p>
<ul>
<li>
<b>kubectl</b>: to manage Kubernetes resources
</li>
<li>
<b>Helm</b>: to build and install the application packages
</li>
</ul>
</p>
<h4>
Usage
</h4>
<p>
To setup the backup resources, you have to launch the script <code>backup/backup_setup.sh</code>
</p>
<p>
The script will perform the following operations:
<ul>
<li>
Install the stash operator
</li>
<li>
Install the stash postgres addon
</li>
<li>
Cleanup the old backup setup if present 
</li>
<li>
Prepare the cluster for backup installation
</li>
<li>
Create a Kubernetes secret to encrypt the backups
</li>
<li>
Create the backup blueprint
</li>
<li>
Copy the secret to access the database from the application namespace
</li>
<li>
Create an appbinding resource to automatically configure the backup
</li>
</ul>
</p>
<h4>The backup process</h4>
<p>
The backup process is detailed in this chart:
<img src="https://gitlab.com/IdleFellow/node-3tier-apps2/-/raw/master/diagrams/Backup%20process.png"/>
</p>
<p>
The process is articulated in the the following steps:
<ul>
<li>
The Stash AppBinding reads the Stash blueprint and automatically creates a Backup Configuration
</li>
<li>
The Backup Configuration creates a CronJob used to schedule the Backup operations
</li>
<li>
The CronJob creates a Job when it's time to start the backup
</li>
<li>
The Job creates a backup pod
</li>
<li>
The backup pod reads a secret to access the database, executes the backup and stores it on an S3 bucket. The backup is encrypted using another secret.
</li>
</ul>
</p>
<p>
The backup process is scheduled everyday around midnight. You can change the schedule time modifying the corresponding information in the blueprint.
</p>
<p>
The backup system is configured to keep the last 5 snapshots. You can change the number of snapshots to be kept modifying the corresponding value in the blueprint.
</p>
<p>
To modify the configuration values, you can simply change them in the setup script <code>backup/backup_setup.sh</code> and re-execute it.<br/>
You only have to be sure not to change the parameter <code>ENCRYPTION_PASSWORD</code> otherwise you will no longer be able to access the old backups.
</p>