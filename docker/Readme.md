<h1>Docker section</h1>
<p>
This section contains the files needed to create the containers needed by the application.
</p>
<h4>
Requirements
</h4>
<p>
<ul>
<li>
<b>Docker</b>: to build the images
</li>
<li>
<b>Docker compose</b> (optional): to test the application locally
</li>
</ul>
</p>
<h4>
Usage
</h4>
<p>
The Dockerfiles included in this section are:
<ul>
<li>
    Dockerfile_api
</li>
<li>
    Dockerfile_web
</li>
<li>
    Dockerfile_db
</li>
</ul>
</p>
<p>
You are not supposed to manage the Dockerfiles directly. The images are built, tagged, pushed and deployed by the Jenkins deployment pipeline.
</p>
<p>
In case you wish to build the image yourself, you can launch this command:<br/>
<code>docker build --target &lt;target&gt; --tag &lt;imageName&gt;:&lt;target&gt; -f &lt;dockerFile&gt; &lt;buildDir&gt;</code>
</p>
<p>
You must provide these parameters:
<ul>
<li>
    <b>target</b>: The application environment: <code>dev</code> or <code>prod</code>
</li>
<li>
    <b>target</b>: The image to buid: <code>node-3tier-app2-web</code>, <code>node-3tier-app2-api</code> or <code>node-3tier-app2-db</code>
</li>
<li>
    <b>dockerFile</b>: the corresponding Dockerfile
</li>
<li>
    <b>target</b>: The buid directory: <code>web</code>, <code>api</code> or an empty dir to build the db image
</li>
</ul>
</p>
<p>
All the Dockerfiles can be built with different targets: <code>dev</code> for develompent builds or <code>prod</code> for images to be installed in production.
</p>
<p>
If you wish to push the images to a Docker repository, remember to tag the images with the labels <code>:dev</code> or <code>:prod</code> according to the environment they are built for.
</p>
<p>
To build and start the containers locally for testing purpose, you can use docker-compose.
</p>
<p>
The commands to use for this purpose are the following:<br/>
<code>docker-compose -f docker/docker_compose.yaml build</code>
to build the images and<br/>
<code>docker-compose -f docker/docker_compose.yaml up</code>
to start the containers
</p>
