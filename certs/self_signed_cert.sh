#!/bin/bash

pushd ${0%/*} >/dev/null

test -f private.key || openssl genrsa 2048 > private.key
openssl req -new -config cert_info.cnf -x509 -nodes -sha1 -days 3650 -extensions v3_ca -key private.key > cert.crt

popd >/dev/null

echo "self signed certificate created"
echo " private key: private.key"
echo " certificate: cert.crt"

