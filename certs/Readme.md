<h1>Certificate section</h1>
<h4>
Requirements
</h4>
<p>
<ul>
<li>
<b>openssl</b> (optional): if you wish to created your own self-signed certificate
</li>
</ul>
</p>
<h4>
Usage
</h4>
<p>
To access the application with the HTTPS protocol, you need to provide an SSL certificate.
</p>
<p>
In a production environment, you should obtain a certificate signed by a well-known CA, but for testing purpose you can simply create a self-signed certificate.<br/>
To create your own certificate, you must launch this command:<br/>
<code>certs/self_signed_cert.sh</code><br/>
The script will create a certificate and a private key.
</p>
<p>
The certificate and the key must be uploaded to the AWS IAM service. Refer to the <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/provisioning">provisioning section</a> for instructions on how to do it.
</p>
<p>
Once you have an SSL certificate and you have uploaded it on the AWS platform, you have to extract its ARN from the AWS console and insert the value in the parameter SSL_CERT_ARN in the Jenkins deployment pipeline.<br/>
In that way, the certificate will be defined in a new SSL service at the next deployment cycle. See the section about <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/jenkins">Jenkins deployment</a> to get more information.
</p>
<p>
Once the new HTTPS services will be in place, you will have to redefine the CDN to let it access the newly created service endpoints.<br/>
Refer to the section <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/provisioning">Provisioning</a> to get more information.
</p> 
<p>
Please be aware that if you plan to use the CDN with SSL services, the SSL certificates of the underlying application services must be valid and signed by a well-known CA. If you use invalid or self-signed certificates, the CDN will not be able to connect to the origin. If you do not have a valid certificate for your domain, you will have to use plain HTTP.
</p>
