#!/bin/bash

pushd ${0%/*}

# Elasticsearch operator
kubectl apply -f https://download.elastic.co/downloads/eck/1.2.1/all-in-one.yaml

# Elasticsearch
kubectl apply -f elasticsearch.yaml

# Kibana
kubectl apply -f kibana.yaml

# Elasticsearch external service
kubectl apply -f elasticsearch_svc.yaml

# Fluentd
# ServiceAccount
kubectl apply -f fluentd_service_account.yaml
# ClusterRole
kubectl apply -f fluentd_cluster_role.yaml
# ClusterRoleBinding
kubectl apply -f fluentd_cluster_role_binding.yaml
# DaemonSet
PASSWORD=$(kubectl get secret elasticsearch-es-elastic-user -o=jsonpath='{.data.elastic}' | base64 --decode)
sed -e "s|_PASSWORD_|${PASSWORD}|" fluentd.yaml | kubectl apply -f -

popd

echo "logging configuration finished"
echo "you can access the Kibana dashboard using this URL: "
echo -n "https://"
kubectl get svc elasticsearch-ext -o=jsonpath='{.status.loadBalancer.ingress[0].hostname}'
echo
echo "and these credentials:"
echo "User: elastic"
echo "Password: $PASSWORD"
