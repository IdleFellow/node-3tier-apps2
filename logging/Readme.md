<h1>Logging section</h1>
<p>
This section contains the information about the log collection and access.
</p>
<h4>
Requirements
</h4>
<p>
<ul>
<li>
<b>kubectl</b>: to manage Kubernetes resources
</li>
</ul>
</p>
<h4>
Usage
</h4>
<p>
To setup the logging resources, you have to launch the script <code>logging/logging_setup.sh</code>
</p>
<p>
The script will perform the following operations:
<ul>
<li>
Install the elasticsearch all-in-one suite (Elastic + Kibana)
</li>
<li>
Create the Elasticsearch resources
</li>
<li>
Create the Kibana resources
</li>
<li>
Create the Elasticsearch service
</li>
<li>
Create the Fluentd ServiceAccount
</li>
<li>
Create the Fluentd ClusterRole
</li>
<li>
Create the Fluentd ClusterRoleBinding
</li>
<li>
Create the Fluentd DaemonSet
</li>
</ul>
</p>
<h4>Additional configuration</h4>
<p>
The Elasticsearch web console will display all the logs of the pods running in the cluster.<br/>
You can browse them or perform search queries.
</p>
<p>
If you wish to access the just the Node.js application logs, in this directory are contained two dashboards that can be loaded into the elasticsearch console:
<ul>
<li>
<b>dev_kibana_dashboard.json</b>: dashboard showing the logs of the development environment pods
</li>
<li>
<b>prod_kibana_dashboard_blue.json</b>: dashboard showing the logs of the production environment pods (blue deployment)
</li>
<li>
<b>prod_kibana_dashboard_green.json</b>: dashboard showing the logs of the production environment pods (blue deployment)
</li>
</ul>
You can load the two ready-made dashboards using the Elasticsearch web interface.
</p>
<p>
Here below, you can see an example dashboard showing the application logs:
<img src="https://gitlab.com/IdleFellow/node-3tier-apps2/-/raw/master/diagrams/Logging%20dashboard%20example.PNG"/>
</p>
