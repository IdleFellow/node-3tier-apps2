var express = require('express');
var router = express.Router();
var request = require('request');


var api_url = process.env.API_HOST + '/api/status';

// monitoring
const client = require('prom-client');
const collectDefaultMetrics = client.collectDefaultMetrics;
const prefix = 'web_';
collectDefaultMetrics({prefix});

// Metrics endpoint
router.get('/metrics', (req, res) => {
  res.set('Content-Type', client.register.contentType)
  res.send(client.register.metrics())
})

/* GET home page. */
router.get('/', function(req, res, next) {
  request(
    {
      method: 'GET',
      url: api_url,
      json: true
    },
    function (error, response, body) {
      if (error || response.statusCode !== 200) {
        return res.status(500).send('error running request to ' + api_url);
      } else {
        res.render('index', {
          title: '3tier App' ,
          request_uuid: body.request_uuid,
          time: body.time
        });
      }
    }
  );
});

module.exports = router;
