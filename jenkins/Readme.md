<h1>Jenkins section</h1>
<p>
This section contains all the information about building and deploying the application.
</p>
<h4>
Requirements
</h4>
<p>
<ul>
<li>
<b>Docker</b>: to build the images and to run the Jenkins container
</li>
<li>
<b>kubectl</b>: to manage Kubernetes resources
</li>
<li>
<b>AWS CLI</b>: to manage cloud resources (you must also setup the default configuration and credentials for the tool)
</li>
<li>
<b>Helm</b>: to build and install the application packages
</li>
</ul>
</p>
<h4>
Usage
</h4>
<p>
<b>TL;DR</b><br/>
Launch the script <code>./jenkins/start.sh</code> and follow the instructions printed at the end ot the output
</p>
<p>
The script will perform these operations:
<ul>
<li>
Build the Jenkins image
</li>
<li>
Start the Jenkins container
</li>
<li>
Read the initial password that must be used at first login
</li>
<li>
Copy the <code>jenkins-cli.jar</code> package inside the running container
</li>
<li>
Provide instructions for the following (unfortunately manual) steps
</li>
</ul>
You are required to perform a couple of manual operations because you will need to define the credentials to access this Git repository from the Jenkins container
</p>
<p>
After executing the script you will have to perform the following operations:
<ul>
<li>
Access the Jenkins application via browser
</li>
<li>
Define the credentials needed to access this Git repository and set the value 'ToptalGit' as credential id
</li>
<li>
Load the deployment scripts inside the container with the following commands:<br/>
<code>docker exec -i jenkins java -jar /var/jenkins_home/jenkins-cli.jar -auth admin:&lt;initial password&gt; -s http://localhost:8080/ create-job deploy-dev &lt; jenkins/config_dev.xml</code><br/><br/>
<code>docker exec -i jenkins java -jar /var/jenkins_home/jenkins-cli.jar -auth admin:&lt;initial password&gt; -s http://localhost:8080/ create-job deploy-prod &lt; jenkins/config_prod.xml</code><br/><br/>
Substitute <code>&lt;initial password&gt;</code> with the password printed by the Jenkins start script
</li>
</ul>
</p>
<p>
The Jenkins container has been provided to simplify all the operations related to build, deployment and delivery of the application.<br/>
The provided container, in fact, contains:
<ul>
<li>
All the required software packages
</li>
<li>
All required Jenkins plugins
</li>
<li>
All the Jenkins configuations already in place (apart from the repository access credentials)
</li>
<li>
The deployment jobs already configured and scheduled for both the development and the production environment.
</li>
</ul>
</p>
<p>
The container is configured with these options:
<ul>
<li>
Container name: jenkins
</li>
<li>
TCP Web Port: 8080
</li>
<li>
The pipeline job deploy-dev scheduled every hour and triggered only when new changes are pushed into the branch <code>develop</code>
</li>
<li>
The pipeline job deploy-prod scheduled every day around midnight and pointing to the branch <code>master</code>
</li>
</ul>
</p>
<p>
You can choose not to use the provided Jenkins container and go for your own installation of the software.<br/>
In that case you must:
<ul>
<li>
Install all the plugins listed in the file <code>jenkins/plugins.txt</code>
</li>
<li>
Deploy the build and deploy jobs as a declarative pipeline that is taken from this Git repository at the path <code>jenkins/Jenkinsfile</code> of the desidered branch
</li>
</ul>
The parameters used by the declarative pipeline are descrived below, together with the details of the steps performed.
</p>
<h4>Deployment pipeline details</h4>
<p>
The declarative pipeline used to build and deploy the application is stored in the file <code>jenkins/Jenkinsfile</code>. It also loads the groovy scripts included in the directory <code>jenkins/scripts</code>.
</p>
<p>
The pipeline accepts the following parameters:
<ul>
<li>
<b>SSL_CERT_ARN</b> (string, default value: ""): The ARN of the SSL certificate
</li>
<li>
<b>DOCKER_REPO</b> (string, default value: doioso): The Docker repository to push the images into
</li>
<li>
<b>KUBECONFIG</b> (string, default value: .kube/config): The file containing the information needed to access the Kubernetes cluster. In case of relative path, it is searched into the home directory of the user jenkins
</li>
<li>
<b>APP_ENVIRONMENT</b> (choice options, default value: dev): The environment for which the application will be built. It can be <code>dev</code> or <code>prod</code>
</li>
<li>
<b>SERVICE_SWITCH_TIMEOUT</b> (string, treated as an integer, default value: 7): The maximum number of days within which you need to activate the new environment in case of rolling deployment into production (see the following section for more details)
</li>
<li>
<b>RECREATE_ENVIRONMENT</b> (boolean, default value: true): Uninstall the existing application before installing the new version
</li>
<li>
<b>DEBUG</b> (boolean, default value: false): Show debug information in the console output
</li>
</ul>
</p>
<p>
The pipeline sets the following environment variables:
<ul>
<li>
<b>APP_NAME</b>: node-3tier-app2
</li>
<li>
<b>DB_APP_IMAGE</b>: postgres:11.2-alpine
</li>
<li>
<b>DB_APP_NAME</b>: node-3tier-db
</li>
<li>
<b>SVC_APP_NAME</b>: blue-green-svc
</li>
<li>
<b>EMPTY_BUILD_DIR</b>: /tmp/empty
</li>
<li>
<b>JENKINS_GIT_DIR</b>: jenkins
</li>
<li>
<b>SCRIPT_DIR</b>: scripts
</li>
<li>
<b>DOCKER_DIR</b>: docker
</li>
<li>
<b>DEPLOY_DIR</b>: deploy
</li>
</ul>
</p>
<p>
The pipeline performs the following steps:
<ul>
<li>
<b>Load scripts</b>: loads the groovy scripts. It loads a script common to all environments named <code>common.groovy</code> and an environment-specific script
</li>
<li>
<b>Set Kubernetes cluster access info</b>: sets the environment variable <code>KUBECONFIG</code> to point to the Kubernetes configuration file
</li>
<li>
<b>Show build information</b>: shows parameters and enviroment variable values (only if the DEBUG option is enabled)
</li>
<li>
<b>Build</b>: builds the Docker images composing the application. The specific Docker target is set to the environment of the build process
</li>
<li>
<b>Push Images</b>: pushes the Docker images to the Docker repository
</li>
<li>
<b>Check deployment status</b>: reads information about the current deployed version of the application
</li>
<li>
<b>App redeploy confirmation</b>: in case we are building for the production environment and a complete redeploy of the application has been chosen, the pipeline asks for user confirmation. If the user do not respond within 1 hour, the process is aborted.
</li>
<li>
<b>Clean</b>: uninstalls the previous version of the application if requested
</li>
<li>
<b>Check namespace status</b>: read information about the existing Kubernates namespace (in case it was not deleted in the step above)
</li>
<li>
<b>Create namespace</b>: creates the Kubernates namespace if needed
</li>
<li>
<b>Get role</b>: gets the application role: <code>dev</code> for development environments, <code>blue</code> or <code>green</code> for production environments (see the section below about rolling deployments)
</li>
<li>
<b>Deploy db</b>: deploys the database application if the old version was uninstalled
</li>
<li>
<b>Deploy application</b>: deploys the Node.js application compoments. If a certificate ARN is provided, the application services will be defined to use the SSL protocol. Otherwise, if the parameter is empty, the services will be configured to use the plain HTTP protocol.
</li>
<li>
<b>Service redirection confirmation</b>: asks the user if they are ready to switch to the new version of the application in case of rolling deployment in a production environment. The user must reply within the number of days passed as parameter. If the timeout expires, the rest of the process is aborted
</li>
<li>
<b>Switch service</b>: in case of rolling deployment in a production environment, the new deployment is activated
</li>
<li>
<b>Undeploy old version confirmation</b>: asks the user if they are ready to uninstall to the old version of the application in case of rolling deployment in a production environment. The user must reply within the number of days passed as parameter. If the timeout expires, the rest of the process is aborted
</li>
<li>
<b>Undeploy old version</b>: uninstalls the old version of the application in case of rolling deployment in a production environment
</li>
<li>
<b>Post install actions</b>: performs cleanup operations.
</li>
</ul>
</p>
<h4>Rolling deployments</h4>
<p>
The production environment is deployed in a continuous fashion without interrupting the service.<br/>
At every production deployment, the new version is installed together with the old. Both versions can be accessed concurrently using special URLs created for the purpose.<br/>
After a new deployment, the public address used by the general public will continue to point to the old version of the application until the Jenkins operator decides to switch it to the new installation.
</p>
<p>
The new environment is initially configured with the minimum possibile number of components of every service. The containers are not even capable of scaling up in case of heavy load. The new environment is intended only as a test installation used to check that everything works as expected.
</p>
<p>
When all the tests are performed and we are confident that the new version is OK, the public URL can be switched to point to the new environment. The switch operation will also take care of setting up the autoscaling options for the new environment and disabling them for the old one.
</p>
<p>
The old environment can be kept for a while to be used as a reference or as a last chance to roll back in case of unexpected problems. When ready, the old version can be completely uninstalled.
</p>
<p>
The deployment pipeline takes automatically care of all these aspects. The only manual interventions are required to confirm the enviroment switch and the removal of the old version of the application.
</p>
<h4>Summing up</h4>
<p>
Refer to the flow-chart below to follow the pipeline process
<img src="https://gitlab.com/IdleFellow/node-3tier-apps2/-/raw/master/diagrams/Jenkins%20pipeline.png"/>
</p>
