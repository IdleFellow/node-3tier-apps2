#!/bin/bash

CONTAINER_NAME=jenkins
APP=node-3tier-app2
DOCKER_GID=$(getent group docker | cut -d ':' -f 3)
WEB_PORT=8080
CONTAINER_WEB_PORT=8080
JAVA_PORT=50000
DOCKER_SOCKET=/var/run/docker.sock
JENKINS_HOME=/var/jenkins_home

DOCKER_CONFDIR=.docker
DOCKER_VOLUME=
test -d "$HOME/$DOCKER_CONFDIR" && DOCKER_VOLUME="-v $HOME/$DOCKER_CONFDIR:$JENKINS_HOME/$DOCKER_CONFDIR"

KUBE_CONFDIR=.kube
KUBE_VOLUME=
test -d "$HOME/$KUBE_CONFDIR" && KUBE_VOLUME="-v $HOME/$KUBE_CONFDIR:$JENKINS_HOME/$KUBE_CONFDIR"

AWS_CONFDIR=.aws
AWS_VOLUME=
test -d "$HOME/$AWS_CONFDIR" && AWS_VOLUME="-v $HOME/$AWS_CONFDIR:$JENKINS_HOME/$AWS_CONFDIR"

pushd ${0%/*} > /dev/null

docker build -t ${APP}-jenkins .
docker run -p $WEB_PORT:$CONTAINER_WEB_PORT -p $JAVA_PORT:$JAVA_PORT \
           $DOCKER_VOLUME $KUBE_VOLUME $AWS_VOLUME \
           --group-add $DOCKER_GID -v $DOCKER_SOCKET:$DOCKER_SOCKET --name=$CONTAINER_NAME \
           -d --restart unless-stopped ${APP}-jenkins

echo -n "starting jenkins ... "
docker exec $CONTAINER_NAME /bin/bash -c "while :; do test -f $JENKINS_HOME/secrets/initialAdminPassword && break; sleep 2; done"
echo "OK"

docker cp jenkins-cli.jar $CONTAINER_NAME:$JENKINS_HOME/
JENKINS_PASSWORD=$(docker exec $CONTAINER_NAME cat $JENKINS_HOME/secrets/initialAdminPassword)

popd > /dev/null

echo "* * *"
test -z "$DOCKER_VOLUME" && echo "docker configuration not found: jenkins may not work correctly" 1>&2
test -z "$KUBE_VOLUME" && echo "kubectl configuration not found: jenkins may not work correctly" 1>&2
test -z "$AWS_VOLUME" && echo "aws cli configuration not found: jenkins may not work correctly" 1>&2
echo "* * *"
echo "jenkins container is up and running."
echo "you can access it at the address http://localhost:$WEB_PORT with the initial password \`$JENKINS_PASSWORD'"
echo
echo "To complete the configuration you have to:"
echo "- create a credential setting to access the Git repository with id \`ToptalGit'"
echo "- import the two pipelines with these commands:"
echo "  docker exec -i $CONTAINER_NAME java -jar $JENKINS_HOME/jenkins-cli.jar -auth admin:$JENKINS_PASSWORD -s http://localhost:8080/ create-job deploy-dev < jenkins/config_dev.xml"
echo "  docker exec -i $CONTAINER_NAME java -jar $JENKINS_HOME/jenkins-cli.jar -auth admin:$JENKINS_PASSWORD -s http://localhost:8080/ create-job deploy-prod < jenkins/config_prod.xml"

