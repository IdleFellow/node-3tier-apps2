def debug(s) {
  if(params.DEBUG) {
      echo s
  }
}

def set_kubeconfig(kubeconfig){
  env.KUBECONFIG=kubeconfig
  if(kubeconfig.substring(0,1) != "/"){
    env.KUBECONFIG=env.HOME + "/" + kubeconfig
  }
}

def build_container(target, dockerFile, buildDir, imageName) {
  sh("mkdir -p " + buildDir)
  sh("docker build --target " + target + " --tag " + imageName + ":" + target + " -f " + dockerFile + " " + buildDir)
}

def push_image(dockerRepo, imageName, appEnvironment) {
  sh("docker image push " + dockerRepo + "/" + imageName + ":" + appEnvironment)
}

def check_deployment_status(appEnvironment, appName) {
  sh(returnStdout: true, script: "/usr/sbin/helm --namespace=" + appEnvironment + " list | sed -ne 's|^\\(" + appName + "[^[:space:]]*\\)[[:space:]].*|\\1|p' | tail -1").trim()
}

def check_namespace_status(appEnvironment) {
  sh(returnStdout: true, script: "kubectl get ns | sed -ne 's|^\\(" + appEnvironment + "\\)[[:space:]].*|\\1|p'").trim()
}

def get_role(appEnvironment, podName, appName) {
  sh(returnStdout: true, script: "kubectl --namespace=" + appEnvironment + " get deployment -l app=" + podName + "-" + appName + " -o json | jq .items[0].spec.selector.matchLabels.role | sed -e 's|^\"" + podName + "-\\([^\"]\\+\\)\"\$|\\1|'").trim()
}

def deploy(deployDir, appEnvironment, operation, appName, appRole, sslCert) {
  releaseName=appName
  roleStr=""
  if(appRole != "") {
    releaseName=appName + "-" + appRole
    roleStr="--set deploymentName=" + appRole
  }

  sslStr=""
  if(sslCert != "") {
    sslStr="--set sslCert=" + sslCert
  }
  
  __deploy(deployDir, appEnvironment, roleStr, sslStr, operation, releaseName, appName)
}

def undeploy(deployDir, appEnvironment, appName, appRole) {
  sh("cd " + deployDir + " && /usr/sbin/helm --namespace=" + appEnvironment + " uninstall " + appName + "-" + appRole)
}

def deploy_db(deployDir, appEnvironment, appName) {
  __deploy(deployDir, appEnvironment, "", "", "install", appName, appName)
}

def deploy_svc(deployDir, appEnvironment, operation, appName, appRole, sslCert) {
  roleStr=""
  if(appRole != "") {
    roleStr="--set deploymentName=" + appRole
  }
  sslStr=""
  if(sslCert != "") {
    sslStr="--set sslCert=" + sslCert
  }

  __deploy(deployDir, appEnvironment, roleStr, sslStr, operation, appName, appName)
}

def switch_service(deployDir, appEnvironment, sslCert, appName, oldRole, newRole) {
  sslStr=""
  if(sslCert != "") {
    sslStr="--set sslCert=" + sslCert
  }
  
  __replace(deployDir, appEnvironment, appName, sslStr, newRole, "true")

  if(oldRole != "" && oldRole != newRole) {
    __replace(deployDir, appEnvironment, appName, sslStr, oldRole, "false")
  }
}

def __replace(deployDir, appEnvironment, appName, sslStr, roleName, switchFlag) {
  sh("cd " + deployDir + " && /usr/sbin/helm --set switchRole=" + switchFlag + " --set environment=" + appEnvironment + " --set deploymentName=" + roleName + " --namespace=" + appEnvironment + " " + sslStr + " upgrade " + appName + "-" + roleName + " " + appName)
}

def __deploy(deployDir, appEnvironment, roleStr, sslStr, operation, releaseName, appName) {
  sh("cd " + deployDir + " && /usr/sbin/helm --set environment=" + appEnvironment + " " + roleStr + " " + sslStr + " --namespace=" + appEnvironment + " " + operation + " " + releaseName + " " + appName)
}

return (this)

