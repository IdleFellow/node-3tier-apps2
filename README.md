<h1>Sample 3tier app</h1>

<h3>
A multi-tier application implemented with Node.js.
</h3>

<p>
The application is designed according to a distributed multi-service architecture. It is deployed using Docker containers in a Kubernetes environment.
</p>
<p>
In particular, this repository provides all the tools needed to deploy and maintain the application on an EKS cluster on AWS cloud.
</p>
<p>
This an overview of the general architecture of the system. For more pictures and charts, refer to the <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/diagrams">Diagram section</a>.
</p>
<p>
<img src="https://gitlab.com/IdleFellow/node-3tier-apps2/-/raw/master/diagrams/General%20architecture.png" alt="General architecture"/>
</p>
<p>
The tools used in this project are the following:
<ul>
<li>
    <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/docker">
    <b>Docker</b>:
    </a>
    to host the components of the application
</li>
<li>
    <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/provisioning">
    <b>Ansible</b>:
    </a>
    as an IaC tool to provision and configure the infrastructure components
</li>
<li>
    <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/provisioning">
    <b>AWS CLI</b>:
    </a>
    to manage cloud resources
</li>
<li>
    <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/jenkins">
    <b>Jenkins</b>:
    </a>
    to handle application deployment pipelines
</li>
<li>
    <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/deploy">
    <b>Helm</b>:
    </a>
    as an installation tool
</li>
<li>
    <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/provisioning">
    <b>Cloudfront</b>:
    </a>
    as a distribution CDN
</li>
<li>
    <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/certs">
    <b>openssl</b>:
    </a>
    to create SSL certificates
</li>
<li>
    <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/backup">
    <b>Stash - Restic</b>:
    </a>
    for database backup
</li>
<li>
    <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/logging">
    <b>Elasticsearch - Fluentd - Kibana</b>:
    </a>
    to collect and display application logs
</li>
<li>
    <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/monitoring">
    <b>Prometheus - Grafana</b>:
    </a>
    to collect and display application metrics
</li>
</ul>
</p>
<p>
Refer to the specific sections above for further information.
</p>
<h4>
Requirements
</h4>
<p>
To build, deploy and manage the application you will need the following tools:
<ul>
<li>
<b>A Unix-like system</b>: to host and launch all the tools in this repository
</li>
<li>
<b>Docker</b>: to build the images
</li>
<li>
<b>Docker compose</b> (optional): to test the application locally
</li>
<li>
<b>Ansible</b>: to provision and manage resources
</li>
<li>
<b>kubectl</b>: to manage Kubernetes resources
</li>
<li>
<b>AWS CLI</b>: to manage cloud resources (you must also setup the default configuration and credentials for the tool)
</li>
<li>
<b>Helm</b>: to build and install the application packages
</li>
<li>
<b>An SSL certificate</b>: to be uploaded to AWS infrastructure (see the <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/certs">certificate section</a> for more information)
</li>
<li>
<b>openssl</b> (optional): if you wish to created your own self-signed certificate
</li>
</ul>
</p>
<h4>
How to access the application
</h4>
<p>
The easiest way to access the application is via the defined CDNs
</p>
<p>
The Ansible playbooks used to setup the application will, among other things, set-up two CDNs:
<ul>
<li>
A CDN to access the web application 
</li>
<li>
A CDN to access the api application 
</li>
</ul>
</p>
<p>
The ansible playbook <code>define_cdn.yml</code> creates the two CDNs and displays the DNS names of each of them.<br/>
To access the application, you can point to one of the URLs defined for each CDN.<br/>
The URL will respond to the HTTPS port (automatically redirecting HTTP traffic to the secure port) using the default certificate provided by the AWS infrastructure.
</p>
<p>
To get more information about the definition of the CDNs, you can refer to the <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/provisioning">provisioning section</a>
</p>
<p>
There are other way to access the application components without passing through the CDN.<br/>
These access modes are related to the rolling deployment feature provided by the CI/CD pipeline.<br/>
This configuration will allow you to keep two versions of the application concurrently active in the production environment. Only one of them (usually the old one) will be accessible using the public URL used by the general public.<br/>
The deployment process will setup another URL that can be used to access the new version of the application for testing purpose.<br/>
To get more information about this configuration, refer to the <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/jenkins">deployment section</a>.
</p>
<p>
The application can be accessed via HTTPS or plain HTTP protocol. <br/>
If you want to activate the SSL service, provide a certificate reference in the Jenkins pipeline options, otherwise plain HTTP will be activated. Specifically, to enable SSL, you have to load a certificate into the AWS IAM service and set the ARN of the certificate in the parameter SSL_CERT_ARN of the Jenkins pipeline.<br/>
For more details about SSL configuration, refer to the sections <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/certs">ssl</a/> or <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/jenkins">deployment</a>.
</p>
<p>
Please be aware that if you plan to use the CDN with SSL services, the SSL certificates of the underlying application services must be valid and signed by a well-known CA. If you use invalid or self-signed certificates, the CDN will not be able to connect to the origin. If you do not have a valid certificate for your domain, you will have to use plain HTTP.<br/>
The URL provided by AWS for the CDN will be always SSL enabled, though. the certificate in use will be provided by the cloud provider for its own URL. If you wish to set a DNS alias to access your resources, you will have to provide a valid certificate for your domain.
</p>
<h4>
Step by step set-up and deployment guide
</h4>
<p>
To install and set-up the application infrastructure and the application itself, you need to follow the steps listed below:
<ul>
<li>
Install and configure the requirements mentioned in the "Requirements" paragraph above
</li>
<li>
Provision the EKS cluster (section <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/provisioning">Provisioning</a>)
</li>
<li>
Define the storage classes of the cluster (section <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/provisioning">Provisioning</a>)
</li>
<li>
Set-up a Jenkins server or use the container provided in this repository (section <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/jenkins">Jenkins</a>)
</li>
<li>
Start a deployment pipeline from the Jenkins server (section <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/jenkins">Jenkins</a>)
</li>
<li>
Define the CDNs to access the application (section <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/provisioning">Provisioning</a>)
</li>
<li>
Set-up database backup (section <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/backup">Backup</a>)
</li>
<li>
Set-up log collection (section <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/logging">Logging</a>)
</li>
<li>
Set-up metrics collection and monitoring (section <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/monitoring">Monitoring</a>)
</li>
</ul>
</p>
