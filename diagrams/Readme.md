<h1>Diagrams</h1>
<p>
This section contains the following diagrams and flow-charts:
<ul>
<li>
<b>Backup process.png</b>: diagram detailing the database backup process
</li>
<li>
<b>General architecture.png</b>: diagram of the application design
</li>
<li>
<b>Jenkins pipeline.png</b>: flow-chart of the Jenkins build and deployment process
</li>
<li>
<b>Logging dashboard example.png</b>: an example dashboard showing the application logs
</li>
<li>
<b>Monitoring dashboard example.png</b>: an example monitoring dashboard showing the application memory usage
</li>
</ul>
</p>
