<h1>Monitoring section</h1>
<p>
This section contains the information about the monitoring of the application.
</p>
<h4>
Requirements
</h4>
<p>
<ul>
<li>
<b>kubectl</b>: to manage Kubernetes resources
</li>
<li>
<b>Helm</b>: to build and install the application packages
</li>
</ul>
</p>
<h4>
Usage
</h4>
<p>
To setup the monitoring resources, you have to launch the script <code>monitoring/monitoring_setup.sh</code>
</p>
<p>
The script will perform the following operations:
<ul>
<li>
Remove the old monitoring resources (if present)
</li>
<li>
Install the prometheus operator
</li>
<li>
Create a servicemonitor for the api component (blue deployment)
</li>
<li>
Create a servicemonitor for the web component (blue deployment)
</li>
<li>
Create a servicemonitor for the api component (green deployment)
</li>
<li>
Create a servicemonitor for the web component (green deployment)
</li>
<li>
Create an external service to access the grafana console
</li>
</ul>
</p>
<h4>Monitoring information</h4>
<p>
The components of the Node.js application have been modified to include the module <code>prom-client</code> and to expose the metrics at the path <code>/metrics</code>
</p>
<p>
These service metrics are collected by prometheus using the servicemonitor instances created by the setup script.
</p>
<p>
The service metrics collected by prometheus are then passed to the visualisation tool grafana
</p>
<p>
You can access the grafana console at the URL displayed by the setup script.<br/>
With that tool, you can define all the dashboards you need to monitor the parameters of your application.
If you wish to start with an example dashboard, you can import the sample dashboard saved in the file <code>monitoring/example_dashboard.json</code><br/>
Here below, you can see a screenshot of the example dashboard:
<img src="https://gitlab.com/IdleFellow/node-3tier-apps2/-/raw/master/diagrams/Monitoring%20dashboard%20example.PNG"/>
</p>
