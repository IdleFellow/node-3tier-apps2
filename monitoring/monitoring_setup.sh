#!/bin/bash

NS=monitoring

TARGET_NS=prod
APP_NAME=node-3tier-app2

pushd ${0%/*}

# delete old resources
{
  /usr/sbin/helm delete --namespace "$NS" prometheus
  kubectl delete ns "$NS"
  kubectl delete crd prometheuses.monitoring.coreos.com;
  kubectl delete crd prometheusrules.monitoring.coreos.com;
  kubectl delete crd servicemonitors.monitoring.coreos.com;
  kubectl delete crd podmonitors.monitoring.coreos.com;
  kubectl delete crd alertmanagers.monitoring.coreos.com;
  kubectl delete crd thanosrulers.monitoring.coreos.com;
  kubectl delete PodSecurityPolicy -l release=prometheus
  kubectl delete ClusterRole -l release=prometheus
  kubectl delete ClusterRoleBinding -l release=prometheus
  kubectl delete MutatingWebhookConfiguration -l release=prometheus
  kubectl delete ValidatingWebhookConfiguration -l release=prometheus
  kubectl delete PodSecurityPolicy -l app.kubernetes.io/instance=prometheus
  kubectl delete ClusterRole -l app.kubernetes.io/instance=prometheus
  kubectl delete ClusterRoleBinding -l app.kubernetes.io/instance=prometheus
  kubectl delete MutatingWebhookConfiguration -l app.kubernetes.io/instance=prometheus
  kubectl delete ValidatingWebhookConfiguration -l app.kubernetes.io/instance=prometheus
  kubectl -n kube-system delete Service -l release=prometheus
} 2>/dev/null

# Install stash operator and postgres addon
/usr/sbin/helm repo add stable https://kubernetes-charts.storage.googleapis.com/
/usr/sbin/helm repo update
kubectl create ns "$NS"
/usr/sbin/helm install --namespace "$NS" prometheus stable/prometheus-operator

# create a servicemonitor for the api component (blue deployment)
kubectl -n $NS apply -f - <<EOF
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
    name: api-$APP_NAME-blue
    labels:
        app: api-$APP_NAME-blue
        release: prometheus
spec:
    selector:
        matchLabels:
            app: api-$APP_NAME-blue
    namespaceSelector:
        matchNames:
        - $TARGET_NS
    endpoints:
    - port: api
      path: /metrics
EOF

# create a servicemonitor for the web component (blue deployment)
kubectl -n $NS apply -f - <<EOF
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
    name: web-$APP_NAME-blue
    labels:
        app: web-$APP_NAME-blue
        release: prometheus
spec:
    selector:
        matchLabels:
            app: web-$APP_NAME-blue
    namespaceSelector:
        matchNames:
        - $TARGET_NS
    endpoints:
    - port: web
      path: /metrics
EOF

# create a servicemonitor for the api component (green deployment)
kubectl -n $NS apply -f - <<EOF
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
    name: api-$APP_NAME-green
    labels:
        app: api-$APP_NAME-green
        release: prometheus
spec:
    selector:
        matchLabels:
            app: api-$APP_NAME-green
    namespaceSelector:
        matchNames:
        - $TARGET_NS
    endpoints:
    - port: api
      path: /metrics
EOF

# create a servicemonitor for the web component (green deployment)
kubectl -n $NS apply -f - <<EOF
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
    name: web-$APP_NAME-green
    labels:
        app: web-$APP_NAME-green
        release: prometheus
spec:
    selector:
        matchLabels:
            app: web-$APP_NAME-green
    namespaceSelector:
        matchNames:
        - $TARGET_NS
    endpoints:
    - port: web
      path: /metrics
EOF


# create an external service for grafana
kubectl -n $NS apply -f - <<EOF
apiVersion: v1
kind: Service
metadata:
  annotations:
    meta.helm.sh/release-name: prometheus
    meta.helm.sh/release-namespace: monitoring
  creationTimestamp: null
  labels:
    app.kubernetes.io/instance: prometheus
    app.kubernetes.io/name: grafana
  name: prometheus-grafana-ext
  selfLink: /api/v1/namespaces/monitoring/services/prometheus-grafana
spec:
  ports:
  - name: service
    port: 80
    protocol: TCP
    targetPort: 3000
  selector:
    app.kubernetes.io/instance: prometheus
    app.kubernetes.io/name: grafana
  sessionAffinity: None
  type: LoadBalancer
status:
  loadBalancer: {}
EOF


popd

echo "monitoring configuration finished"

echo "you can access the Grafana dashboard using this URL: "
echo -n "http://"
kubectl -n $NS get svc prometheus-grafana-ext -o=jsonpath='{.status.loadBalancer.ingress[0].hostname}'
echo
echo "and these credentials:"
echo "User: admin"
echo "Password: prom-operator"

