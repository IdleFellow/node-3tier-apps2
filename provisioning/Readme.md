<h1>Provisioning section</h1>
<p>
The provisioning of all the infrastructure resources needed by the application is entirely managed by Ansible.<br/>
This section contains all the playbooks and roles used for the application set-up.
</p>
<h4>
Requirements
</h4>
<p>
<ul>
<li>
<b>Ansible</b>: to provision and manage resources
</li>
<li>
<b>kubectl</b>: to manage Kubernetes resources
</li>
<li>
<b>AWS CLI</b>: to manage cloud resources (you must also setup the default configuration and credentials for the tool)
</li>
<li>
<b>An SSL certificate</b>: to be uploaded to AWS infrastructure (see the <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/certs">certificate section</a> for more information)
</li>
</ul>
</p>
<p>
<b>Please note</b> that installing and setting up the AWS CLI and kubectl is out of the scope of this document.
</p>
<h4>
Usage
</h4>
<p>
This section explains the purpose of the playbooks defined to provision the infrastructure components and how to use them.
</p>
<h5>EKS provisioning</h5>
<p>
To create the EKS cluster and the related resources, you need to launch the playbook <code>eks_provision.yml</code> with the following syntax:<br />
<code>ansible-playbook [-t debug] playbooks/eks_provision.yml</code><br/>
The playbook performs the following operations:
<ul>
<li>
Creates an IAM role for the cluster
</li>
<li>
Creates a VPC and one subnet per Availability Zone
</li>
<li>
Creates a Security Group and associates it to the VPC 
</li>
<li>
Creates the EKS cluster
</li>
<li>
Create an IAM role for cluster nodes
</li>
<li>
Creates a local kubeconfig file with cluster information
</li>
<li>
Creates cluster nodes 
</li>
</ul>
This is the first playbook that must be executed. It serves as a prerequisite for all the others.
</p>
<h5>Storage definition</h5>
<p>
The playbook <code>define_storage.yml</code> is used to create all the storage-related resources needed by the Kubernetes cluster.<br/>
Among other tasks, the playbooks is capable of selecting specific EBS storage volumes and associate them to Kubernetes Persistent Volumes. If you already have some EBS volumes that you wish to use, you simply have to tag them setting the key <code>eks_cluster_volume</code> with the name of the cluster (default: <code>toptal-eks</code>)<br/>
It the playbooks cannot find any available EBS volumes, it will create a new one.<br/>
To launch the playbook, use the following command:<br/>
<code>ansible-playbook [-t debug] playbooks/define_storage.yml</code><br/>
The playbook performs the following operations:
<ul>
<li>
Selects available EBS volumes
</li>
<li>
Creates a new EBS volume if no selectable volumes are available
</li>
<li>
Creates a Kubernetes Storage Class
</li>
<li>
Creates a Kubernetes Persistent Volume for every available EBS volume and associates it to the Storage Class
</li>
</ul>
</p>
<h5>SSL certificate upload</h5>
<p>
The playbook <code>upload_certificate.yml</code> is used to upload an SSL certificate to the AWS IAM service.<br/>
It can be launched with the following syntax:<br/>
<code>ansible-playbook [-t debug] playbooks/upload_certificate.yml</code><br/>
The playbook performs the following operations:
<ul>
<li>
Selects available EBS volumes
</li>
<li>
Uploads an SSL certificate to the AWS IAM service
</li>
<li>
Shows the certificate ARN
</li>
</ul>
The certificate and the CA private key must be previously uploaded in the directory <code>certs</code>, see the variables at the top of the playbook file to read or modify the file names. To create a self-signed certificate for testing purpose, check the <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/certs">certificate section</a> for more information.<br />
After the SSL certificate upload, you will have to insert the certificate ARN in the key <code>sslCert</code> defined in the file <code>deploy/blue-green-svc/values.yaml</code>. See the <a href="https://gitlab.com/IdleFellow/node-3tier-apps2/-/tree/master/deploy">deployment section</a> for more information.
</p>
<h5>CDN definitions</h5>
<p>
To define the two CDNs with Cloudfront, you need to launch the playbook <code>define_cdn.yml</code> with the following syntax:<br />
<code>ansible-playbook [-t debug] playbooks/define_cdn.yml</code><br/>
The playbook performs the following operations:
<ul>
<li>
Read the DNS name of the web service (ssl or plain http)
</li>
<li>
Read the DNS name of the api service (ssl or plain http)
</li>
<li>
Disables and deletes the old web CDN with Cloudfront
</li>
<li>
Defines the web CDN with Cloudfront
</li>
<li>
Disables and deletes the old api CDN with Cloudfront
</li>
<li>
Defines the api CDN with Cloudfront
</li>
<li>
Shows the two DNS names configured for the CDNs
</li>
</ul>
The CDNs are created using newly generated DNS names and will be served using the SSL protocol with the certificate provided by the cloud provider for its own infrastructure.<br/>
Please be aware that if you plan to use the CDN with your own SSL services with the underlying application, the SSL certificates must be valid and signed by a well-known CA. If you use invalid or self-signed certificates, the CDN will not be able to connect to the application. If you do not have a valid certificate for your domain, you will have to use plain HTTP for the origin. The user-facing CDN will still be able to use SSL, though<br/>
Finally, if you wish to set a DNS alias of the CDN URL to access your resources, you will have to provide a valid certificate for your domain.
</p>
<h4>Playbook notes</h4>
<p>
Following the usual Ansible IaC style, all the playbooks are idempotent. This means that all the playbooks can be executed several times without side effects that could mess up your infrastructure.<br/>
The playbook <code>eks_provision.yml</code> must be executed first. All the other playbooks depend on it.
</p>
<h4>Ansible configuration</h4>
<p>
All the playbooks can be customised editing the variables contained at the top of the playbook file or those contained in the file <code>group_vars/all/common.yml</code>
</p>
